var data;
var count = 0;
var temp;

function user(id)
{

    if (window.XMLHttpRequest) {
        myAjax = new XMLHttpRequest();
    } else {
        myAjax = new ActiveXObject("Microsoft.XMLHTTP");
    }
 
    myAjax.open("GET","https://jsonplaceholder.typicode.com/users/"+id,true);
    myAjax.send();

    myAjax.onreadystatechange=function () {

        if (temp == undefined) {
            temp = document.getElementById("users").innerHTML;
        }

        document.getElementById("users").innerHTML = temp;

        if (myAjax.readyState == 4 && myAjax.status == 200) {
            data = JSON.parse(myAjax.responseText);
            document.getElementById("users").rows[id].cells[1].innerHTML =
            data.name + " <br>" +
            "EMail: " + data.email + " <br>" +
            "Street: " + data.address.street + " <br>" +
            "Suite: " + data.address.suite + " <br>" +
            "City: " + data.address.city + " <br>" +
            "Zipcode: " + data.address.zipcode + " <br>" +
            "Geo: " + data.address.geo.lat + ", " + data.address.geo.lng + " <br>";
            document.getElementById("users").rows[id].cells[2].innerHTML =
            data.username + "<br>" +
            "Phone: " + data.phone + " <br>" +
            "Website: " + data.website + " <br>" +
            "Company: <br>" +
            data.company.name + " <br>" +
            data.company.catchPhrase + " <br>" +
            data.company.bs;
        }

        if (myAjax.readyState == 0) {
            alert("Error");
        }
        
        
    }
}
