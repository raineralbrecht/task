<?php
declare(strict_types = 1);
namespace task\publish\php\classes;

class Json
{
    private $url;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    public function fetch(): string
    {
        $json = file_get_contents($this->url);
        return $json;
    }
}
