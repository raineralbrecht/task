<?php
declare(strict_types = 1);
namespace task\publish\php\classes;

class Task
{
    public function __construct()
    {
    }

    public function activate()
    {
    }

    public function deactivate()
    {
    }

    public function uninstall()
    {
    }

    public function endpoint()
    {
        global $wp_query;
        if (isset($wp_query->query_vars['users'])) {
            add_filter('the_content', [$this, 'content']);
        }
    }

    public function endpointCreate()
    {
        add_rewrite_endpoint('users', EP_PERMALINK | EP_PAGES);
    }

    public function content()
    {
        require_once 'Json.php';
        $json = new Json('https://jsonplaceholder.typicode.com/users');
        $data = json_decode($json->fetch());
        echo '<table id="users" class="users">';
        echo '<tr><td>ID</td><td>Name</td><td>User</td></tr>'."\n";
        foreach ($data as $result) {
            $id = $result->id;
            $name = $result->name;
            $username = $result->username;
            echo '<tr><td>'.esc_html($id).'</td>';
            echo '<td><a href="javascript: onclick=user('.esc_html($id).')">'.
            esc_html($name).'</a></td>'."\n";
            echo '<td><a href="javascript: onclick=user('.esc_html($id).')">'.
            esc_html($username).'</a></td></tr>'."\n";
        }
        echo '</table>'."\n";
    }
}
