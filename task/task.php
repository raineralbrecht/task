<?php
declare(strict_types = 1);
/*
Plugin Name: Task
Description: Task from Inpsyde
Version: 1.0.2
Author: Rainer Albrecht
License: GPLv2 or later
D:\Workspace PHP\Task\vendor\bin>phpcs --standard=inpsyde ..\..\task\public
*/
// Exit if accessed directly.
defined('ABSPATH') or die('Hello');

wp_register_script('main', plugins_url('publish/js/Ajax.js', __FILE__));
wp_register_style('main', plugins_url('publish/css/style.css', __FILE__));
wp_enqueue_script('main');
wp_enqueue_style('main');
include_once 'publish/php/classes/Task.php';

if (class_exists('task\publish\php\classes\Task')) {
    $task = new task\publish\php\classes\Task();
}

// activation
register_activation_hook(__file__, [$task, 'activate']);

// deactivation
register_deactivation_hook(__file__, [$task, 'deactivate']);

add_action('init', [$task, 'endpointCreate']);
add_action('template_redirect', [$task, 'endpoint']);
